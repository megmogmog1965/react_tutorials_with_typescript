var dest = './static';
var build = './build';
var src = './src';
var path = require('path');
var relativeSrcPath = path.relative('.', src);

module.exports = {
  tsc: {
    src: src + '/**/*.{ts,tsx}',
    dest: build
  },

  copy: {
    src: src,
    dest: dest
  },

  browserify: {
    entry: {
      entries: build + '/ts/app.js',
      debug: true
    },
    dest: build + '/js',
    output: {
      filename: 'bundle.js'
    }
  },

  minify: {
    src: build + '/js/bundle.js',
    filename: 'bundle.js',
    dest: dest + '/js'
  },

  clean: {
    dirs: [dest, build]
  }
}
