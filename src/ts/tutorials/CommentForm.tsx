import * as React from 'react';

import * as Types from './Types';


export interface Props {
  onCommentSubmit?: (comment: Types.TComment) => void;
}

export interface State {
  onCommentSubmit?: (comment: Types.TComment) => void;
}

export class CommentForm extends React.Component<Props, State> {

  private _author: React.HTMLAttributes;
  private _text: React.HTMLAttributes;

  constructor() {
    super();
  }

  private _handleSubmit(e): void  {
    e.preventDefault();
    let author = this._author.value.trim();
    let text = this._text.value.trim();
    if (!text || !author) {
      return;
    }

    // call callback.
    this.props.onCommentSubmit({ author: author, text: text });
    this._author.value = '';
    this._text.value = '';

    return;
  }

  render() {
    return (
      <form className='commentForm' onSubmit={ this._handleSubmit.bind(this) } >
        <input type='text' placeholder='Your name' ref={ (ref) => this._author = ref } />
        <input type='text' placeholder='Say something...' ref={ (ref) => this._text = ref } />
        <input type='submit' value='Post' />
      </form>
    );
  }
}
