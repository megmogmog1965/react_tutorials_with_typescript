import * as React from 'react';

import { Comment } from './Comment';
import * as Types from './Types';


export interface Props {
  data: Types.TComment[];
}

export interface State {
}

export class CommentList extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
  }

  render() {
    let commentNodes = this.props.data.map(
      (comment) => {
        return (
          <Comment author={comment.author}>
            {comment.text}
          </Comment>
        );
    });

    return (
      <div className='commentList'>
        {commentNodes}
      </div>
    );
  }
}
