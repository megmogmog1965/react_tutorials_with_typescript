import * as React from 'react';


export interface Props {
  children?: any;
  author?: string;
}

export interface State {
  author: string;
}

declare function marked(s: string, opt?: any): any;

export class Comment extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = { author: props.author ? props.author : '' };
  }

  private _rawMarkup() {
    let rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  }

  render() {
    return (
      <div className='comment'>
        <h2 className='commentAuthor'>
          {this.props.author}
        </h2>
        <span dangerouslySetInnerHTML={ this._rawMarkup() } />
      </div>
    );
  }
}
