
module Types {
  export interface TComment {
    author: string;
    text: string;
  }
}


export = Types;
