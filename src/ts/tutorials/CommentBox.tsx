import * as React from 'react';

import { CommentList } from './CommentList';
import { CommentForm } from './CommentForm';
import * as Types from './Types';


export interface Props {
  url: string;
  pollInterval: number;
}

export interface State {
  data: Types.TComment[];
}

export class CommentBox extends React.Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = { data: [] };
  }

  getInitialState(): State {
    return { data: [] };
  }

  componentDidMount(): void {
    this._loadCommentsFromServer();
    setInterval(this._loadCommentsFromServer.bind(this), this.props.pollInterval);
  }

  private _loadCommentsFromServer(): void {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,

      success: data => {
        this.setState({ data: data.comments });
      },

      error: (xhr, status, err) => {
        console.error(this.props.url, status, err.toString());
      }
    });
  }

  private _handleCommentSubmit(comment: Types.TComment) {
    let comments = this.state.data;
    comments.push(comment);
    this.setState({ data: comments });

    $.ajax({
      type: 'POST',
      url: this.props.url,
      contentType: 'application/json',
      dataType: 'json',
      data: JSON.stringify(comment),

      success: (data: Types.TComment) => {
        // nothing to be done.
      },

      error: (xhr, status, err) => {
        console.error(this.props.url, status, err.toString());
      }
    });
  }

  render() {
    return (
      <div className='commentBox'>
        <h1>Comments</h1>
        <CommentList data={this.state.data} />
        <CommentForm onCommentSubmit={ this._handleCommentSubmit.bind(this) } />
      </div>
    );
  }
}
