/*
 * main.c みたいなもの.
 */

import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { CommentBox } from './tutorials/CommentBox';


ReactDOM.render(
  <CommentBox url='/apis/comments' pollInterval={ 2000 } />,
  document.getElementById('react-content')
);
