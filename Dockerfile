FROM centos:centos7
MAINTAINER Yusuke Kawatsu "https://bitbucket.org/megmogmog1965"

# Set correct environment variables.
ENV HOME /root
ENV LANG en_US.UTF-8
ENV PATH ${HOME}/.nodebrew/current/bin:${PATH}

# install commands.
RUN rpm -iUvh http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm && \
    yum -y update && \
    yum -y install curl python-pip python-devel npm && \
    yum -y clean all

# install python modules.
RUN pip install flask

# expose ports.
EXPOSE 5000

# ADD python scripts.
ADD . /project_files

# build scripts.
RUN cd /project_files && \
    npm install && \
    npm run build

# entrypoint.
ENTRYPOINT python /project_files/flaskserver.py
