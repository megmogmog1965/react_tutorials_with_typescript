# Description

[Tutorial | React]を[TypeScript]を使ってやってみた。

# How to build & start

Nodeがまだ入っていない場合はinstallする.

* [Node.js + TypeScript + Atom 環境を構築する]

サーバーサイドAPIを[Flask]で書いているので、[Flask]をinstallする。

```
$ pip install flask
```

.ts, .tsxをビルドする。

```
$ npm install
$ npm run build
```

[Flask]サーバーを起動する。

```
$ npm run start
```

# run by docker

```
$ docker build -t react_tutorial .
$ docker run -d -p 5000:5000 --name react_tutorial react_tutorial
```

# 開発・実行環境

* OS: Mac OS 10.11.1
* Editor: [Atom]
    * [Node.js + TypeScript + Atom 環境を構築する]

# Author

[Yusuke Kawatsu]


[React]:https://facebook.github.io/react/
[TypeScript]:http://www.typescriptlang.org/
[Flask]:http://flask.pocoo.org/
[Atom]:https://atom.io/
[Tutorial | React]:https://facebook.github.io/react/docs/tutorial.html
[Node.js + TypeScript + Atom 環境を構築する]:http://qiita.com/megmogmog1965/items/0e443b7f49ede24cde7b
[Yusuke Kawatsu]:https://bitbucket.org/megmogmog1965
